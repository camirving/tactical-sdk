﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class CustomWeapon_Flamethrower : WeaponBase
{
	private ParticleSystem _fireParticles;
	private AudioSource _audioFireIgnite;
	private AudioSource _audioFireLoop;
	private AudioSource _audioFireEnd;
	private TextMeshPro _textDynamic;
	private GameObject _textParentObject;

	private bool _shouldEmitFlame;
	private bool _wasEmittingFlamesLastFrame;
	private int _ammoLastFrame;

	public override void Initialize()
	{
		_fireParticles = Descriptor.AdditionalObjects[3].GetComponent<ParticleSystem>();
		_audioFireIgnite = Descriptor.AdditionalObjects[0].GetComponent<AudioSource>();
		_audioFireLoop = Descriptor.AdditionalObjects[1].GetComponent<AudioSource>();
		_audioFireEnd = Descriptor.AdditionalObjects[2].GetComponent<AudioSource>();
		_fireParticles.GetComponent<ParticleCallbackBypasser>().OnCollision.AddListener(OnParticleHit);
		_textDynamic = Descriptor.AdditionalObjects[4].GetComponent<TextMeshPro>();
		_textParentObject = _textDynamic.transform.parent.gameObject;
	}

	public override void InitializeNetVars()
	{
	}

	public override void OnWeaponEquip()
	{
	}

	public override void OnWeaponAim()
	{
		_audioFireIgnite.Play();
	}

	public override void Tick()
	{
		_textParentObject.gameObject.SetActive(IsWeaponHolder());
		if (IsWeaponHolder())
		{
			int ammo = GetAmmo();

			if (ammo != _ammoLastFrame)
			{
				_textDynamic.text = ammo.ToString();
				if (ammo < Descriptor.MaxAmmo / 20)
				{
					_textDynamic.color = Color.red;
				}
				else
				{
					_textDynamic.color = Color.white;
				}
			}

			_ammoLastFrame = GetAmmo();
		}
	}

	public override void FixedTick()
	{
		_shouldEmitFlame = false;

		if (GetAmmo() <= 0)
		{
			if (_wasEmittingFlamesLastFrame != _shouldEmitFlame)
			{
				OnEmissionChanged();
			}

			_wasEmittingFlamesLastFrame = _shouldEmitFlame;
			return;
		}

		float lastAttackTime = GetLastAttackTime();

		if (Time.time < lastAttackTime + .1f)
		{
			_shouldEmitFlame = true;
			ReduceAmmo(1);
		}

		if (_shouldEmitFlame)
		{
			_fireParticles.Emit(1);
		}

		if (_wasEmittingFlamesLastFrame != _shouldEmitFlame)
		{
			OnEmissionChanged();
		}

		_wasEmittingFlamesLastFrame = _shouldEmitFlame;
	}

	void OnEmissionChanged()
	{
		if (_shouldEmitFlame)
		{
			_audioFireIgnite.Stop();
			_audioFireLoop.Play();
		}
		else
		{
			_audioFireLoop.Stop();
			_audioFireEnd.Play();
		}
	}

	public override void Shoot()
	{
		if (EnoughTimePassedSinceLastAttack())
		{
			int ammo = GetAmmo();

			if (ammo <= 0)
			{
				PlayAudioNoAmmo();
				return;
			}

			RegisterAttackTime();
		}
	}

	public override void Reload(bool finishedReloading)
	{
		throw new System.NotImplementedException();
	}

	void OnParticleHit(GameObject arg0)
	{
		RaycastHit rhit = new RaycastHit();
		rhit.normal = Descriptor.RaycastTransform.forward * -1;
		
		TryDealDamage(arg0.GetComponent<Collider>(), rhit);
	}
}