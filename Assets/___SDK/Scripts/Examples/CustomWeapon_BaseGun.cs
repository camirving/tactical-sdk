﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomWeapon_BaseGun : WeaponBase
{
	// These vars are needed for the burst fire capabilities
	private int _burstRemainingShots;
	private Coroutine _burstRoutine;
	private WaitForSeconds _burstWait;

	public override void Initialize()
	{
		// If this is a burst fire weapon, we must initialize a burst fire coroutine
		if (Descriptor.BurstFireCount > 1)
		{
			_burstWait = new WaitForSeconds(Descriptor.BurstFireTimeInterval);
			_burstRoutine = StartCoroutine(BurstRoutine());
		}
	}

	public override void InitializeNetVars()
	{
	}

	public override void OnWeaponEquip()
	{
	}

	public override void OnWeaponAim()
	{
	}

	public override void Tick()
	{
	}

	public override void FixedTick()
	{
	}

	public override void Shoot()
	{
		// Has enough time passed since our last attack?
		if (EnoughTimePassedSinceLastAttack())
		{
			// If yes, let's register the current time as the last time of attack.
			RegisterAttackTime();

			int ammo = GetAmmo();

			// Do we have enough ammo?
			if (ammo <= 0)
			{
				PlayAudioNoAmmo();
				return;
			}

			if (Descriptor.BurstFireCount > 1)
			{
				if (ammo >= Descriptor.BurstFireCount)
				{
					_burstRemainingShots = Descriptor.BurstFireCount;
				}
				else
				{
					_burstRemainingShots = ammo;
				}
			}
			else
			{
				ReduceAmmo(1);

				// This fires the gun.
				Fire();
			}
		}
	}

	public override void Reload(bool finishedReloading)
	{
	}

	private void Fire()
	{
		DoRaycasts();

		GetRibbonParticle().Play();
		GetSmokeParticle().Play();
		SpawnCasing();
		DoMuzzleFlash();

		if (IsWeaponHolder())
		{
			DoScreenShakeAndRecoil();
		}
		else
		{
			DoWeaponFireAnimation();
		}

		PlayAudioFire();
	}

	private void DoRaycasts()
	{
		for (int i = 0; i < Descriptor.PelletCount; i++)
		{
			Transform raycastTransform = GetRaycastTransform();

			Vector3 direction = raycastTransform.forward;

			float spreadAmount = Descriptor.PelletSpread;

			float spreadX = Random.RandomRange(-spreadAmount, spreadAmount);
			float spreadY = Random.RandomRange(-spreadAmount, spreadAmount);

			Vector3 spreadVector = new Vector3(spreadX, spreadY, 0f);
			spreadVector = raycastTransform.TransformDirection(spreadVector);

			direction += spreadVector;
			direction.Normalize();

			Ray ray = new Ray(raycastTransform.position, direction);

			RaycastHit rayHit;

			bool raycast = Physics.Raycast(ray, out rayHit, Mathf.Infinity, GetDamageMask());

			if (raycast)
			{
				Collider col = rayHit.collider;

				if (col != null)
				{
					TryDealDamage(col, rayHit);
				}

				SpawnTracer(rayHit);
			}
			else
			{
				SpawnTracer(direction);
			}
		}
	}

	IEnumerator BurstRoutine()
	{
		yield return null;

		while (true)
		{
			if (_burstRemainingShots > 0)
			{
				Fire();
				ReduceAmmo(1);
				_burstRemainingShots--;
				yield return _burstWait;
			}
			yield return null;
		}
	}
}