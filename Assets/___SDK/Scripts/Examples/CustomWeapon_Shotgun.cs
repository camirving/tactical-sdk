﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomWeapon_Shotgun : WeaponBase
{
	private AudioSource _pumpAudioSource;

	private const string COCKED = "bool_cocked";

	public override void Initialize()
	{
		_pumpAudioSource = Descriptor.AdditionalObjects[0].GetComponent<AudioSource>();
	}

	public override void InitializeNetVars()
	{
		SetNetBool(COCKED, true);
	}

	public override void OnWeaponEquip()
	{
	}

	public override void OnWeaponAim()
	{
	}

	private bool IsCocked()
	{
		return GetNetBool(COCKED);
	}

	private void SetCocked(bool b)
	{
		if (IsWeaponHolder())
		{
			SetNetBool(COCKED, b);
		}
	}

	public override void FixedTick()
	{
	}

	public override void Shoot()
	{
		// Has enough time passed since our last attack?
		if (EnoughTimePassedSinceLastAttack())
		{
			// If yes, let's register the current time as the last time of attack.
			RegisterAttackTime();

			// Do we have enough ammo?
			if (GetAmmo() <= 0)
			{
				PlayAudioNoAmmo();
				return;
			}

			if (IsCocked())
			{
				SetCocked(false);

				ReduceAmmo(1);

				Fire();

				GetRibbonParticle().Play();
				GetSmokeParticle().Play();
				DoMuzzleFlash();

				if (IsWeaponHolder())
				{
					DoScreenShakeAndRecoil();
				}
				else
				{
					DoWeaponFireAnimation();
				}

				PlayAudioFire();
			}
			else
			{
				SetCocked(true);
				_pumpAudioSource.Play();
				SpawnCasing();
			}
		}
	}

	private void Fire()
	{
		for (int i = 0; i < Descriptor.PelletCount; i++)
		{
			Transform raycastTransform = GetRaycastTransform();

			Vector3 direction = raycastTransform.forward;

			float spreadAmount = Descriptor.PelletSpread;

			float spreadX = Random.RandomRange(-spreadAmount, spreadAmount);
			float spreadY = Random.RandomRange(-spreadAmount, spreadAmount);

			Vector3 spreadVector = new Vector3(spreadX, spreadY, 0f);
			spreadVector = raycastTransform.TransformDirection(spreadVector);

			direction += spreadVector;
			direction.Normalize();

			Ray ray = new Ray(raycastTransform.position, direction);

			RaycastHit rayHit;

			bool raycast = Physics.Raycast(ray, out rayHit, Mathf.Infinity, GetDamageMask());

			if (raycast)
			{
				Collider col = rayHit.collider;

				if (col != null)
				{
					TryDealDamage(col, rayHit);
				}

				SpawnTracer(rayHit);
			}
			else
			{
				SpawnTracer(direction);
			}
		}
	}

	public override void Tick()
	{
	}

	public override void Reload(bool finishedReloading)
	{
	}
}