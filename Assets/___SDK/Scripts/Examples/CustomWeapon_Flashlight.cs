﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomWeapon_Flashlight : WeaponBase
{
	private Light _light;
	private bool _isLit;

	private const string LIT = "bool_lit";

	public override void Initialize()
	{
		_light = Descriptor.AdditionalObjects[0].GetComponent<Light>();
		_isLit = GetNetBool(LIT);
	}

	public override void InitializeNetVars()
	{
		SetNetBool(LIT, false);
	}

	public override void OnWeaponEquip()
	{
	}

	public override void OnWeaponAim()
	{
	}

	public override void FixedTick()
	{
	}

	public override void Shoot()
	{
		if (EnoughTimePassedSinceLastAttack())
		{
			RegisterAttackTime();

			_isLit = GetNetBool(LIT);

			PlayAudioFire();

			if (_isLit)
			{
				if (IsWeaponHolder())
				{
					SetNetBool(LIT, false);
				}
			}
			else
			{
				if (IsWeaponHolder())
				{
					SetNetBool(LIT, true);
				}
			}
		}
	}

	public override void Tick()
	{
		_light.enabled = _isLit;
	}

	public override void Reload(bool finishedReloading)
	{
	}
}