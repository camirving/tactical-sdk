﻿public enum WeaponHoldType
{
	NONE,
	RIFLE,
	PISTOL,
	FLASHLIGHT = 7
}

public enum AmmoType
{
	STANDARD557,
	STANDARD9MM,
	MAGNUM,
	ACP45,
	BUCKSHOT,
	CELLS
}

public enum Effects
{
	BURN,
	SLOW,
	DIZZY
}