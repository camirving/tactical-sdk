﻿using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class SimpleEvent : UnityEvent<GameObject>
{
	
}

public class ParticleCallbackBypasser : MonoBehaviour
{
    public SimpleEvent OnCollision;

    void Awake()
    {
        OnCollision = new SimpleEvent(); 
    }
	
    private void OnParticleCollision(GameObject other)
    {
        OnCollision.Invoke(other);
    }
}