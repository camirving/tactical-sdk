﻿using System.Collections;
using System.Collections.Generic;
using Ionic.Zip;
using UnityEditor;
using UnityEngine;
using UnityEngine.Windows;

public class WeaponPublisher : EditorWindow
{
	private List<WeaponDescriptor> _weapons;
	private Vector2 _scrollPos;

	private string _progressInfo;
	private float _progressValue;
	private bool _shouldShowProgressBar;

	[MenuItem("SDK/Publish Weapons")]
	public static void ShowWindow()
	{
		var w = GetWindow<WeaponPublisher>();
	}

	void CollectAllWeapons()
	{
		var objs = Selection.gameObjects;

		_weapons = new List<WeaponDescriptor>();

		foreach (GameObject o in objs)
		{
			WeaponDescriptor w = o.GetComponent<WeaponDescriptor>();

			if (w != null)
			{
				_weapons.Add(w);
			}
		}
	}

	void Update()
	{
		CollectAllWeapons();

		Repaint();
	}

	private void OnGUI()
	{
		if (_weapons == null) return;

		GUILayout.Label("Selected weapon count: " + _weapons.Count, EditorStyles.helpBox);

		_scrollPos = GUILayout.BeginScrollView(_scrollPos, false, true);

		foreach (WeaponDescriptor w in _weapons)
		{
			GUILayout.Label(w.ItemName);
		}

		GUILayout.EndScrollView();

		GUILayout.Space(30);

		if (_weapons.Count > 0)
		{
			if (GUILayout.Button("Publish weapons", EditorStyles.toolbarButton))
			{
				PublishAllWeapons();
			}
		}

		GUILayout.Space(15);

		if (_shouldShowProgressBar)
		{
			EditorUtility.DisplayProgressBar("Please wait...", _progressInfo, _progressValue);
		}
	}

	private void PublishAllWeapons()
	{
		_shouldShowProgressBar = true;

		string localPath = "Assets/Temp";

		if (Directory.Exists(localPath))
		{
			Directory.Delete(localPath);
		}

		_progressInfo = "Preparing all weapons...";

		foreach (WeaponDescriptor w in _weapons)
		{
			w.Prepare();
		}

		_progressInfo = "Creating Prefabs";

		Directory.CreateDirectory(localPath);

		List<TextAsset> scripts = new List<TextAsset>();

		foreach (WeaponDescriptor w in _weapons)
		{
			string prefabPath = localPath + "/" + w.ItemName + ".prefab";

			if (AssetDatabase.LoadAssetAtPath(prefabPath, typeof(GameObject)))
			{
				AssetDatabase.DeleteAsset(prefabPath);
			}

			if (scripts.Contains(w.Script) == false)
			{
				scripts.Add(w.Script);
			}

			GameObject prefab = PrefabUtility.SaveAsPrefabAsset(w.gameObject, prefabPath);
			AssetImporter.GetAtPath(prefabPath).SetAssetBundleNameAndVariant("bundle", "");
		}

		_progressInfo = "Creating AssetBundle";

		string bundlePath = localPath + "/Bundles";

		Directory.CreateDirectory(bundlePath);

		BuildPipeline.BuildAssetBundles(bundlePath, BuildAssetBundleOptions.ForceRebuildAssetBundle, BuildTarget.StandaloneWindows64);

		string path = EditorUtility.SaveFilePanel("Save weapon as .mod", "", "weapons.mod", "mod");


		using (ZipFile zip = new ZipFile())
		{
			zip.AddFile(bundlePath + "/bundle", "");
			zip.AddFile(bundlePath + "/bundle.manifest", "");

			foreach (TextAsset script in scripts)
			{
				zip.AddFile(AssetDatabase.GetAssetPath(script), "");
			}

			zip.Password = GeneratePassword();
			zip.Save(path);
		}

		Directory.Delete(localPath);

		AssetDatabase.Refresh();

		_shouldShowProgressBar = false;
	}

	// TODO: make this method actually generate a password lmao
	public string GeneratePassword()
	{
		return "aeiou";
	}
}