﻿using UnityEngine;

// This is WeaponBase, the base class for all weapons.
// All weapons should inherit from this, obviously.
public abstract class WeaponBase : MonoBehaviour
{
#region [Vars]

	// This is our weapon's descriptor. Use it to fetch info from the descriptor, such as Max Ammo, Damage, etc.
	public WeaponDescriptor Descriptor { get; private set; }

#endregion

#region [API Functions]

	// Has enough time passed since we last attacked?
	public bool EnoughTimePassedSinceLastAttack()
	{
		return true;
	}

	// Am I the server?
	public bool IsServer()
	{
		return true;
	}

	// Am I the player currently holding this weapon?
	public bool IsWeaponHolder()
	{
		return true;
	}

	// How many rounds are left in our gun?
	public int GetAmmo()
	{
		return 0;
	}
	
	public float GetLastAttackTime()
	{
		return 0;
	}

	// Returns the transform used for raycasting.
	public Transform GetRaycastTransform()
	{
		return Descriptor.RaycastTransform;
	}

	// Returns the layermask used for damaging things.
	public LayerMask GetDamageMask()
	{
		return 0;
	}

	public ParticleSystem GetRibbonParticle()
	{
		return Descriptor.ParticlesSmokeRibbon;
	}

	public ParticleSystem GetSmokeParticle()
	{
		return Descriptor.ParticlesFire;
	}

	// Register the current time as the last attack time. Should be called whenever you shoot.
	public void RegisterAttackTime()
	{
	}

	// Reduces the gun's ammo by amount.
	public void ReduceAmmo(int amount)
	{
	}

	// Spawns a bullet tracer with rHit's info. It will die at rHit.point
	public void SpawnTracer(RaycastHit rHit)
	{
	}

	// Spawns a bullet tracer that will head in the input direction. The direction must be in world space. Use this when you've hit nothing with your shot.
	public void SpawnTracer(Vector3 direction)
	{
	}

	public void SpawnCasing()
	{
	}

	public void DoMuzzleFlash()
	{
	}

	public void DoScreenShakeAndRecoil()
	{
	}

	// Makes our capsuleperson move his little arms, as if they were shooting their gun.
	public void DoWeaponFireAnimation()
	{
	}

	// Deals damage to collider, if collider is a player or a breakable world entity.
	// Returns true if damage has been dealt to something, false if nothing received damage.
	public bool TryDealDamage(Collider col, RaycastHit rhit)
	{
		return true;
	}

	// Play the "No Ammo" sound.
	public void PlayAudioNoAmmo()
	{
	}

	// Play the "Fire" sound.
	public void PlayAudioFire()
	{
	}

#endregion

#region [Networked Vars]

	// These are net var commands. use them to get/set networked vars.
	public string GetNetString(string key)
	{
		return null;
	}

	public void SetNetString(string key, string value)
	{
	}

	public void SetNetBool(string key, bool value)
	{
	}

	public bool GetNetBool(string key)
	{
		return false;
	}

	public void SetNetInt(string key, int value)
	{
	}

	public int GetNetInt(string key)
	{
		return 0;
	}

	public void SetNetFloat(string key, float value)
	{
	}

	public float GetNetFloat(string key)
	{
		return 0f;
	}

#endregion

#region [Hooks]

	// Called when the gun is first initialized.
	// Use this to cache other components (GetComponent) and other initialization routines.
	// This is called on ALL players: weapon holder, other clients, the server, EVERYONE
	public abstract void Initialize();

	// Called when the gun is first initialized by the server. Should be used to initialize netvars.
	// All networked variables must be first initialized inside this method.
	// After that, everyone can GET the vars but only the weapon holder can SET them
	// This is ONLY CALLED on the server.
	public abstract void InitializeNetVars();
	
	// Called for everyone when this weapon is equipped (that is, someone is now visibly holding this gun)
	public abstract void OnWeaponEquip();

	// Called for everyone when someone is taking aim with this weapon.
	public abstract void OnWeaponAim();

	// Called on every frame for all players.
	public abstract void Tick();
	
	// Called on every fixed frame.
	public abstract void FixedTick();

	// Called when the gun fires, for all players.
	public abstract void Shoot();

	// Called when the gun starts or ends being reloaded, for all players.
	public abstract void Reload(bool finishedReloading);

#endregion
}