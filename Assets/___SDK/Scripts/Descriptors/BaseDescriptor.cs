﻿using System;
using UnityEngine;

public abstract class BaseDescriptor : MonoBehaviour
{
	[Header("Base Descriptor Settings")]
	public string ItemName;
	public string AuthorName;
	[TextArea(3, 10)]
	public string ItemDescription;

	public Guid GUID { get; private set; }


	public void GenerateUniqueID()
	{
		GUID = Guid.NewGuid();
		Debug.Log("Generated Unique ID. " + GUID);
	}

	public virtual void Prepare()
	{
		GenerateUniqueID();
	}
}