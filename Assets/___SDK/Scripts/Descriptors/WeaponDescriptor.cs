﻿
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor.Experimental.SceneManagement;
using UnityEditor.SceneManagement;
#endif
using UnityEngine;

public class WeaponDescriptor : BaseDescriptor
{
	[Header("Transforms")]
	public Transform RenderTransform;
	public Transform ColliderTransform;
	public Transform RaycastTransform;
	public Transform LaserSightStartTransform;
	public Transform CasingSpawnTransform;

	[Header("Settings - Weapon")]
	public TextAsset Script;
	[SerializeField]
	[HideInInspector]
	public string ScriptName;
	[Range(1, 3)]
	public int Slot;
	public WeaponHoldType HoldType;
	public int Damage = 15;
	public List<Effects> DamageEffects;
	public int PelletCount = 1;
	[Range(0f, 1f)]
	public float PelletSpread = 0f;
	public float FireInterval = 0.11f;
	public bool IsAutomatic = true;
	[Tooltip("If this value is higher than one, the weapon will burstfire.")]
	[Range(1, 999)]
	public int BurstFireCount = 1;
	[Tooltip("Time between each burst round")]
	public float BurstFireTimeInterval = 0.02f;
	public bool UsesAmmo = true;
	public int MaxAmmo = 30;
	public AmmoType AmmoKind;
	public float ReloadDuration = 1.6f;
	public bool ReloadRoundsIndividually;
	public float AimAdjustRate = 8f;
	[Tooltip("This will multiply the base damage according to the distance. Time is distance and value is the multiplier.")]
	public AnimationCurve DistanceFalloffCurve;
	public float KnockbackForce = 100;
	public Vector2 RecoilIntensity = new Vector2(2, -4);
	[SerializeField]
	[HideInInspector]
	public Vector3 HoldOffset = new Vector3(-0.05f, 0, 0.4f);
	public Vector3 ShakeIntensity = new Vector3(0.2f, 0.2f, 0);
	public float ShakeTime = 0.1f;


	[Header("Settings - Laser")]
	public Color LaserColor;

	[Header("Settings - MuzzleFlash")]
	public bool UseMuzzleFlash = true;
	public SpriteRenderer[] MuzzleFlashSprites;
	public Light MuzzleFlashLight;

	[Header("Settings - Particles & Smoke")]
	public ParticleSystem ParticlesFire;
	public ParticleSystem ParticlesSmokeRibbon;

	[Header("Settings - Audio")]
	public AudioSource AudioFire;
	public AudioSource AudioEmpty;
	public AudioSource AudioReloadStart;
	public AudioSource AudioReloadEnd;

	[Header("Settings - Extra")]
	public GameObject[] AdditionalObjects;

	[SerializeField]
	[HideInInspector]
	public AudioSource[] AudioSources;


	[ContextMenu("Prepare")]
	public override void Prepare()
	{
		var c = GetComponentsInChildren<AudioSource>();
		AudioSources = GetComponentsInChildren<AudioSource>(true);

		HoldOffset = transform.localPosition;
		ScriptName = Script.name;

#if UNITY_EDITOR

		var prefabStage = PrefabStageUtility.GetCurrentPrefabStage();
		if (prefabStage != null)
		{
			EditorSceneManager.MarkSceneDirty(prefabStage.scene);
		}
#endif

		base.Prepare();
	}
}